set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo
" Plugin 'tpope/vim-fugitive'
" plugin from http://vim-scripts.org/vim/scripts.html
" Plugin 'L9'
" Git plugin not hosted on GitHub
" Plugin 'git://git.wincent.com/command-t.git'
" git repos on your local machine (i.e. when working on your own plugin)
" Plugin 'junegunn/fzf'
" Plugin 'junegunn/fzf.vim'
 
" Plugin 'Valloric/YouCompleteMe'

Plugin 'dpelle/vim-LanguageTool'

Plugin 'dense-analysis/ale'

Plugin 'roxma/nvim-yarp'

Plugin 'roxma/vim-hug-neovim-rpc'

Plugin 'Shougo/deoplete.nvim'

Plugin 'vim-syntastic/syntastic'

Plugin 'nvie/vim-flake8'

Plugin 'vim-airline/vim-airline'

Plugin 'junegunn/rainbow_parentheses.vim'
let g:rainbow#pairs = [['(', ')'], ['[', ']'], ['{', '}']]

Plugin 'hzchirs/vim-material'

Plugin 'scrooloose/nerdtree'

Plugin 'sheerun/vim-polyglot'

Plugin 'joshdick/onedark.vim'

Plugin 'easymotion/vim-easymotion'

Plugin 'davidhalter/jedi-vim'

Plugin 'tpope/vim-fugitive'

Plugin 'yggdroot/indentline'

Plugin 'rhysd/vim-clang-format'

Plugin 'kien/ctrlp.vim'

Plugin 'majutsushi/tagbar'

Plugin 'vim-latex/vim-latex'

" The sparkup vim script is in a subdirectory of this repo called vim.
" Pass the path to set the runtimepath properly.
Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
" Install L9 and avoid a Naming conflict if you've already installed a
" different version somewhere else.
" Plugin 'ascenator/L9', {'name': 'newL9'}

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

let g:languagetool_jar='/home/robert/Documents/languagetool/LanguageTool-4.7/languagetool-commandline.jar'

let g:tex_flavor='latex'
"let g:vimtex_quickfix_mode=0
let g:tex_conceal='abdmgs'

let g:deoplete#enable_at_startup = 0

function! Tab_Or_Complete()
  if col('.')>1 && strpart( getline('.'), col('.')-2, 3 ) =~ '^\w'
    return "\<C-N>"
  else
    return "\<Tab>"
  endif
endfunction
:inoremap <Tab> <C-R>=Tab_Or_Complete()<CR>

"let g:ale_completion_enabled=1

"let g:clang_c_options = '-std=c11'
"let g:clang_cpp_options = '-std=c++17 -stdlib=libc++' 

" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line" All system-wide defaults are set in $VIMRUNTIME/archlinux.vim (usually just
"
" This is the default extra key bindings

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

"
set conceallevel=1
let g:indentLine_concealcursor=""
set number
set autoindent
set cindent
inoremap { {<CR>}<up><end><CR>
set tabstop=2
set expandtab
set shiftwidth=2
set softtabstop=2

set splitright
set encoding=utf-8
set clipboard=unnamedplus
set cursorline
set completeopt+=preview

autocmd BufNewFile,BufRead *.tex set spell 
let g:syntastic_tex_checkers = []

let g:Tex_DefaultTargetFormat='pdf'

" /usr/share/vim/vimfiles/archlinux.vim) and sourced by the call to :runtime
" you can find below.  If you wish to change any of those settings, you should
" do it in this file (/etc/vimrc), since archlinux.vim will be overwritten
" everytime an upgrade of the vim packages is performed.  It is recommended to
" make changes after sourcing archlinux.vim since it alters the value of the
" 'compatible' option.

" This line should not be removed as it ensures that various options are
" properly set to work with the Vim-related packages.
runtime! archlinux.vim

" If you prefer the old-style vim functionalty, add 'runtime! vimrc_example.vim'
" Or better yet, read /usr/share/vim/vim80/vimrc_example.vim or the vim manual
" and configure vim to your own liking!

" do not load defaults if ~/.vimrc is missing
"let skip_defaults_vim=1
syntax on
colorscheme onedark


nmap <F7> :NERDTreeToggle <CR>
nmap <F8> :TagbarToggle <CR>

:hi Conceal ctermfg=70 
