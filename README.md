# vim-config

My vim configuration file (vimrc) 

# Setup

Just move the vimrc file either to /etc/vimrc or to ~/.vimrc.
To install all plugins, Vundle has to be installed first. The installation guide
for that can be found under https://github.com/VundleVim/Vundle.vim